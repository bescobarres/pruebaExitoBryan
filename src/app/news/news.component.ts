import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { config } from '../config';
import {MatCardModule} from '@angular/material/card';

@Component({
  selector: 'app-news',
  templateUrl: './news.component.html',
  styleUrls: ['./news.component.css']
})
export class NewsComponent implements OnInit {


  ngOnInit() {
  }

  urlService:string = 'https://newsapi.org/v2/top-headlines?country=us&category=business&apiKey=';
  dataNews: any;
  constructor(private http: HttpClient){
    this.findNews();
  }

  findNews(){
    this.http.get(this.urlService + config.newsApi.key).subscribe((response) => {
      this.dataNews = response;
      return this.dataNews;
    })
  }
}
