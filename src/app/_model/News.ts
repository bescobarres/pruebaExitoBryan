import {Articles} from './Articles';
export class News{
  public status: string;
  public totalResults:string;
  public articles: Array<Articles>;
}
